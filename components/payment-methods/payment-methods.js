define(['knockout', 'text!./payment-methods.html'], function(ko, template) {

	return {
		viewModel: function viewModel(params) {
				var self = this;
				self.header = ko.observable('Payment');
				self.subheader = ko.observable('All transactions are secure & encrypted');
				self.giftcardHeader = ko.observable('Have A Gift Card?')
				// pass through the 'route' parameter to the view
				self.route = params.route;
				self.useAsBillAddress = ko.observable(true);
				self.states = ko.observable(['Alberta', 'Ontario', 'British Columbia']);
				self.saveCard = ko.observable(true);
				self.savedCards = ko.observable([
					{
						"nameOnCard": "Maddula Dinesh Reddy",
						"expiration": "03/23",
						"cardNumber": "**** **** **** 3434",
						"lastFourDigits": "3434",
						"billingAddress": "Canada"
					},
					{
						"nameOnCard": "Maddula Dinesh Reddy",
						"expiration": "03/23",
						"cardNumber": "**** **** **** 7002",
						"lastFourDigits": "7002",
						"billingAddress": "Canada"
					}
				]);
				self.isChecked = function() {
					console.log('clicked', self.useAsBillAddress());
					return true;
				}
			},
		template: template
	};

});
