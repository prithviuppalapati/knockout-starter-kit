define(['knockout', 'text!./shipping-address.html'], function(ko, template) {

	return {
		viewModel: function viewModel(params) {
				var self = this;
				self.shipToMe = ko.observable(true);
				self.toggletabs = function(selected) {
					self.shipToMe(selected);
				}
			},
		template: template
	};

});