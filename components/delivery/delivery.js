define(['knockout', 'text!./delivery.html', 'jquery'], function(ko, template, $) {

	return {
		viewModel: function viewModel(params) {
				var self = this;
				self.showScheduler = ko.observable(true);
				self.header = ko.observable('Delivery Time - What\'s Convenient For You?');
				// self.valueString1 = ko.observable();
				$( "#datepicker" ).datepicker({
					dateFormat: "DD, MM d",
					minDate: new Date(),
					maxDate: "+3m",
					nextText: "Next",
					prevText: "Prev"
				});
		},
		template: template
	};

});
