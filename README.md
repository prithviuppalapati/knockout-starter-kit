# knockout-js-boilerplate
Simple boilerplate project for a knockout js application with separated components loaded by require js.

# Run Locally
1.npm run watch
2.npm run start

# Development

## New route/page
1. Create a new folder under pages/
2. Create respective JS/HTML files
3. Register the route at app/router.js and app/startup.js

## New Component/widget
1. create a folder under component/. for eg.checkout-cart-summary
2. create respective JS/HTML/LESS files
3. Register the route at app/startup.js

