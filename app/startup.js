define(['knockout', 'crossroads', 'hasher', 'jquery', 'jqueryUI', './router'],
		function(ko, crossroads, hasher, jquery, jqueryUI, router) {

	// components
	ko.components.register('payment-methods', { require: 'components/payment-methods/payment-methods' });
	ko.components.register('delivery', { require: 'components/delivery/delivery' });
	ko.components.register('header-component', { require: 'components/header/header' });
	ko.components.register('footer-component', { require: 'components/footer/footer' });
	ko.components.register('checkout-cart-summary', { require: 'components/checkout-cart-summary/checkout-cart-summary' });
	ko.components.register('shipping-address', { require: 'components/shipping-address/shipping-address' });


	// pages
	ko.components.register('checkout-page', { require: 'pages/checkout-page/checkout' });
	ko.components.register('cart-page', { require: 'pages/cart-page/cart' });
	ko.components.register('home-page', { require: 'pages/home-page/home' });

	ko.applyBindings({ route: router.currentRoute });
});
