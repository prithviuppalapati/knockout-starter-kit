var fs = require('fs');
var path = require('path');
var gulp = require('gulp');
var less = require('gulp-less');
var htmlreplace = require('gulp-html-replace');
var chalk = require('chalk');
var clean = require('gulp-clean');
var connect = require('gulp-connect');
var cleanCSS = require('gulp-clean-css');
var concat = require('gulp-concat');
var watchLess = require('gulp-watch-less');

const lessEntities = ['components/**/*.less', 'pages/**/*.less', 'style/style.less'];

gulp.task('less', function() {
    return gulp.src(lessEntities)
        .pipe(less())
        .pipe(concat('index.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest('./dist'));
})

gulp.task('watch', function() {
    gulp.watch(lessEntities, ['less']);
})

gulp.task('html', function() {
    return gulp.src('./index.html')
        .pipe(htmlreplace({
            'css': 'index.css'
        }))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('clean', function() {
    return gulp.src('./dist/**/*', { read: false })
        .pipe(clean());
});

gulp.task('default', ['html', 'watch', 'less'], function(callback) {
    callback();
    console.log('\nPlaced optimized files in ' + chalk.magenta('dist/\n'));
});

gulp.task('serve:dist', ['default'], function() {
    return connect.server({ root: './dist' });
});
